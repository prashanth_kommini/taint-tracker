<?php

require_once 'AspisMain.php';
function redirect_to($new_location)
{
    header(deconcat1('Location: ', $new_location));
    die;
}
function mysql_prep($string)
{
    global $connection;
    $escaped_string = Aspis_mysql_real_escape_string($string);
    return $escaped_string;
}
function confirm_query($result_set)
{
    if (denot_boolean($result_set)) {
        die(deAspis(AspisPrintGuard(array('Database query failed.', false))));
    }
}
function form_errors($errors = array(array(), false))
{
    $output = array('', false);
    if (!(empty($errors) || Aspis_empty($errors))) {
        $output = concat2($output, '<div class="error">');
        $output = concat2($output, 'Please fix the following errors:');
        $output = concat2($output, '<ul>');
        foreach ($errors[0] as $key => $error) {
            restoreTaint($key, $error);
            $output = concat2($output, '<li>');
            $output = concat($output, AspisKillTaint(Aspis_htmlentities($error), 0));
            $output = concat2($output, '</li>');
        }
        $output = concat2($output, '</ul>');
        $output = concat2($output, '</div>');
    }
    return $output;
}
function find_all_subjects()
{
    $subject_set = $GLOBALS[0]['subjects'];
    return $subject_set;
}
function find_pages_for_subject($subject_id)
{
    $page_set = array(array(), false);
    $int_id = array(deAspis(Aspis_intval($subject_id)) - 1, false);
    arrayAssignAdd($page_set[0][], addTaint(attachAspis($GLOBALS[0]['pages'], $int_id[0])));
    return $page_set;
}
function find_all_admins()
{
    global $connection;
    $query = array('SELECT * ', false);
    $query = concat2($query, 'FROM admins ');
    $query = concat2($query, 'ORDER BY username ASC');
    $admin_set = attAspis(mysql_query($query[0]));
    confirm_query($admin_set);
    return $admin_set;
}
function find_subject_by_id($subject_id)
{
    $int_id = array(deAspis(Aspis_intval($subject_id)) - 1, false);
    $subject = attachAspis($GLOBALS[0]['subjects'], $int_id[0]);
    if ($subject[0] != null) {
        return $subject;
    } else {
        return array(null, false);
    }
}
function find_page_by_id($page_id)
{
    $int_id = array(deAspis(Aspis_intval($page_id)) - 1, false);
    $page = attachAspis($GLOBALS[0]['pages'], $int_id[0]);
    if ($page[0] != null) {
        return $page;
    } else {
        return array(null, false);
    }
}
function find_admin_by_username($username)
{
    global $connection;
    $safe_username = Aspis_mysql_real_escape_string($username);
    $query = array('SELECT * ', false);
    $query = concat2($query, 'FROM admins ');
    $query = concat($query, concat2(concat1('WHERE username = \'', $safe_username), '\' '));
    $query = concat2($query, 'LIMIT 1');
    $admin_set = attAspis(mysql_query($query[0]));
    confirm_query($admin_set);
    if (deAspis($admin = attAspisRC(mysql_fetch_assoc($admin_set[0])))) {
        return $admin;
    } else {
        return array(null, false);
    }
}
function find_default_page_for_subject($subject_id)
{
    $page_set = find_pages_for_subject($subject_id);
    return attachAspis($page_set, 0);
}
function find_selected_page()
{
    global $current_subject;
    global $current_page;
    if (isset($_GET[0]['subject']) && Aspis_isset($_GET[0]['subject'])) {
        echo deAspis(AspisPrintGuard(array('SUBJECT IS SET', false), 'Echo Stmt', array()));
        $current_subject = find_subject_by_id($_GET[0]['subject']);
        echo deAspis(AspisPrintGuard(array('PAGES', false), 'Echo Stmt', array()));
        var_dump(deAspisRC($GLOBALS[0]['pages']));
        if ($current_subject[0]) {
            echo deAspis(AspisPrintGuard(array('IN CURRENT_PAGE SET WITHIN GET SUBJECT SET', false), 'Echo Stmt', array()));
            $current_page = find_default_page_for_subject($current_subject[0]['id']);
        } else {
            $current_page = array(null, false);
        }
    } elseif (isset($_GET[0]['page']) && Aspis_isset($_GET[0]['page'])) {
        echo deAspis(AspisPrintGuard(array('PAGE IS SET', false), 'Echo Stmt', array()));
        $current_subject = array(null, false);
        $current_page = find_page_by_id($_GET[0]['page']);
    } else {
        echo deAspis(AspisPrintGuard(array('NEITHER PAGE NOR SUBJECT IS SET', false), 'Echo Stmt', array()));
        $current_subject = array(null, false);
        $current_page = array(null, false);
    }
    echo deAspis(AspisPrintGuard(array('VARDUMPING FROM FIND SELECTED PAGE END
', false), 'Echo Stmt', array()));
    var_dump(deAspisRC($current_page));
}
function navigation($subject_array, $page_array)
{
    echo deAspis(AspisPrintGuard(concat2(concat1('Current page = ', $page_array[0]['menu_name']), '
'), 'Echo Stmt', array('page_array' => $page_array)));
    $output = array('<ul class="subjects">', false);
    $subject_set = find_all_subjects();
    foreach ($subject_set[0] as $subject) {
        $output = concat2($output, '<li');
        if ($subject_array[0] && deAspis($subject[0]['id']) == deAspis($subject_array[0]['id'])) {
            $output = concat2($output, ' class="selected"');
        }
        $output = concat2($output, '>');
        $output = concat2($output, '<a href="index.php?subject=');
        $output = concat($output, Aspis_urlencode($subject[0]['id']));
        $output = concat2($output, '">');
        $output = concat($output, AspisKillTaint(Aspis_htmlentities($subject[0]['menu_name']), 0));
        $output = concat2($output, '</a>');
        $page_set = find_pages_for_subject($subject[0]['id'], array(false, false));
        $output = concat2($output, '<ul class="pages">');
        foreach ($page_set[0] as $page) {
            $output = concat2($output, '<li');
            if ($page_array[0] && deAspis($page[0]['id']) == deAspis($page_array[0]['id'])) {
                $output = concat2($output, ' class="selected"');
            }
            $output = concat2($output, '>');
            $output = concat2($output, '<a href="index.php?page=');
            $output = concat($output, Aspis_urlencode($page[0]['id']));
            $output = concat2($output, '">');
            $output = concat($output, AspisKillTaint(Aspis_htmlentities($page[0]['menu_name']), 0));
            $output = concat2($output, '</a></li>');
        }
        $output = concat2($output, '</ul></li>');
    }
    $output = concat2($output, '</ul>');
    return $output;
}
function public_navigation($subject_array, $page_array)
{
    $output = array('<ul class="subjects">', false);
    $subject_set = find_all_subjects();
    foreach ($subject_set[0] as $subject) {
        $output = concat2($output, '<li');
        if ($subject_array[0] && deAspis($subject[0]['id']) == deAspis($subject_array[0]['id'])) {
            $output = concat2($output, ' class="selected"');
        }
        $output = concat2($output, '>');
        $output = concat2($output, '<a href="index.php?subject=');
        $output = concat($output, Aspis_urlencode($subject[0]['id']));
        $output = concat2($output, '">');
        $output = concat($output, AspisKillTaint(Aspis_htmlentities($subject[0]['menu_name']), 0));
        $output = concat2($output, '</a>');
        if (deAspis($subject_array[0]['id']) == deAspis($subject[0]['id']) || deAspis($page_array[0]['subject_id']) == deAspis($subject[0]['id'])) {
            $page_set = find_pages_for_subject($subject[0]['id']);
            $output = concat2($output, '<ul class="pages">');
            foreach ($page_set[0] as $page) {
                $output = concat2($output, '<li');
                if ($page_array[0] && deAspis($page[0]['id']) == deAspis($page_array[0]['id'])) {
                    $output = concat2($output, ' class="selected"');
                }
                $output = concat2($output, '>');
                $output = concat2($output, '<a href="index.php?page=');
                $output = concat($output, Aspis_urlencode($page[0]['id']));
                $output = concat2($output, '">');
                $output = concat($output, AspisKillTaint(Aspis_htmlentities($page[0]['menu_name']), 0));
                $output = concat2($output, '</a></li>');
            }
            $output = concat2($output, '</ul>');
        }
        $output = concat2($output, '</li>');
    }
    $output = concat2($output, '</ul>');
    return $output;
}
function password_encrypt($password)
{
    $hash_format = array('$2y$10$', false);
    $salt_length = array(22, false);
    $salt = generate_salt($salt_length);
    $format_and_salt = concat($hash_format, $salt);
    $hash = Aspis_crypt($password, $format_and_salt);
    return $hash;
}
function generate_salt($length)
{
    $unique_random_string = attAspis(md5(uniqid(deAspisRC(attAspis(mt_rand())), true)));
    $base64_string = Aspis_base64_encode($unique_random_string);
    $modified_base64_string = Aspis_str_replace(array('+', false), array('.', false), $base64_string);
    $salt = Aspis_substr($modified_base64_string, array(0, false), $length);
    return $salt;
}
function password_check($password, $existing_hash)
{
    $hash = Aspis_crypt($password, $existing_hash);
    if ($hash[0] === $existing_hash[0]) {
        return array(true, false);
    } else {
        return array(false, false);
    }
}
function attempt_login($username, $password)
{
    $admin = find_admin_by_username($username);
    if ($admin[0]) {
        if (deAspis(password_check($password, $admin[0]['hashed_password']))) {
            return $admin;
        } else {
            return array(false, false);
        }
    } else {
        return array(false, false);
    }
}
function logged_in()
{
    return array(isset($_SESSION[0]['admin_id']) && Aspis_isset($_SESSION[0]['admin_id']), false);
}
function confirm_logged_in()
{
    if (denot_boolean(logged_in())) {
        redirect_to(array('login.php', false));
    }
}
?>

